//logical operators



//logical AND (&&)
// returns true if the both the operands are TRUE

// let highincome =true;
// let cibilscore = true;
// let eligibleperson = highincome && cibilscore
//  console.log("status:" + eligibleperson);



//logical OR (||)
// returns true if any one of the operand is TRUE
// let highincome =true;
// let cibilscore = false;
// let eligibleperson = highincome || cibilscore

// console.log("status:" + eligibleperson);



//not(!)
// let highincome =true;
// let cibilscore = false;
// let eligibleperson = highincome || cibilscore
// let applicationstatus = !eligibleperson

 
// console.log("application status:" + applicationstatus);



// logical operator without non-boolean


let usercolor= undefined;
let defaultcolor="white";
let presentcolor = usercolor || defaultcolor;
console.log (presentcolor);
