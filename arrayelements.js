// adding elements in array

// const number =["red","blue","pink"]

// // number.push ("grey")

// number.unshift("yellow")

// number.splice (3,0, "white", "green")
// console.log(number);

//  finding element in primitive datatype

const colors =["red","green","white","red"]
// console.log(colors.lastIndexOf("red"));
 console.log (colors.includes("grey"))


//  finding element in referece datatypes

const orders=[
    {id: 1, item: "smartphone" ,Number: "1"},
    {id: 2, item: "iphone" ,Number: "1"},
    

]

//   let result = orders.find((order)=> order.item ==="iphone")


//  console.log(result);


// //  removing a element in an array
// // ~ pop 
// // ~ shift
// // ~splice

// const number = [1,2,3,4];
// number.splice (2,2)
// console.log(number);

// emptying an js array

// let number =[1,2,3,4];

//   while (number.length) number.pop();
//   console.log(number);


// combining arrays

const shopping_cart = [{item: "maggi"}];
const additional_cart =["Matchbox","salt","beans","masala"];


// // combine
// let recipe = shopping_cart.concat(additional_cart);
// shopping_cart[0].item ="noodles" 

// // extract
//  let extract = recipe.slice(1,3)
//  console.log(recipe);
//  console.log(extract);


// spread operator
//  let recipe =[...shopping_cart,45,...additional_cart];
//  console.log(recipe);

//  iteraring arrays
 
const dailyroutine =["wake up", "eat", "sleep"];

//  for-of
// for( let routine of dailyroutine){
//     // logics
//     console.log(routine);
// }
// //  for in 
// for (let routine in dailyroutine){
//     console.log(dailyroutine[routine]);

// }

// for each
// dailyroutine.forEach(routine => console.log(routine));
    
// const swetha = [ "eat", "play", "sleep"];
// swetha.forEach(routine => console.log(routine));

// joining

const swetha =["sleep", "eat","play"];
let activities = swetha.join (" , ");
console.log(activities);


// let fullname = "swetha saran";
// let name = fullname.split (" ");
// let firstname = name[0];
// let lastname =name[1];
// console.log(` my firstname is ${firstname} and last name is ${lastname}`);




let title = " this is js ";
 let post = title.split(" ");
  let finalpost = post.join('_');
  
console.log( finalpost);



